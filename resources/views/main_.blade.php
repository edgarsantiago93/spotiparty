<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <!-- Styles -->
    <style>


        html, body {
            /*background-color:#1C1F24;*/
            /*background-color:#0A1731;*/
            background-color: #222d43;
            color: #fff;
            /*color: #636b6f;*/
            font-family: 'Nunito', sans-serif;
            font-weight: 200;
            height: 100vh;
            margin: 0;
        }


        .nopadding {
            padding: 0;
        }

        .t_cent {
            text-align: center;
        }

        .nomargin {
            margin: 0;
        }

        .songtitle {
            font-weight: bold;
            font-size: 16px;
            line-height: 36px;
        }

        .addtokiu {
            line-height: 60px;
            font-size: 60px;
        }
    </style>
</head>
<body>


<section>
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12" style="text-align:center;">
                <button onclick="newSession()" class="btn">LaRichiLuismiPeda</button>
            </div>
        </div>
        {{--<div class="col-xs-12" style="text-align:center;">--}}
            {{--<button onclick="newSession()" class="btn">Crear</button>--}}
        {{--</div>--}}
    </div>
</section>
</body>
<script src="https://code.jquery.com/jquery-3.2.1.js" integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE="
        crossorigin="anonymous"></script>

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
        integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
        crossorigin="anonymous"></script>

<script src="https://unpkg.com/axios/dist/axios.min.js"></script>

<script>

    function newSession(){
        window.location.href = '{{url('new_session')}}'
    }

    function joinSession(w) {
        $('#join_div').fadeIn();
        if(w === 1){
            axios.get('/join_session?id='+$('#join_id').val()).then(function (response) {
                console.log(response);
            })
                .catch(function (error) {
                    console.log(error);
                });
        }
    }

</script>
</html>

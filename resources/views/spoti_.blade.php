<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Felicidades Josuuuuun</title>
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="{{asset('css/animate.css')}}">
    <!-- Styles -->
    <style>
        html, body {
            /*background-color:#1C1F24;*/
            /*background-color:#0A1731;*/
            /*background-color: #222d43;*/
            background-color: #081733;
            color: #fff;
            /*color: #636b6f;*/
            font-family: 'Nunito', sans-serif;
            font-weight: 200;
            height: 100vh;
            margin: 0;
            touch-action: manipulation;

        }

        .nopadding {
            padding: 0;
        }

        .t_cent {
            text-align: center;
        }

        .nomargin {
            margin: 0;
        }

        .songtitle {
            font-weight: bold;
            font-size: 16px;
            line-height: 36px;
        }

        .addtokiu {
            line-height: 70px;
            font-size: 60px;
            width: 100%;
        }

        .loader_ {
            width: 100%;
            max-width: 35px;
            display: none;
            margin: 0 auto;
        }

        .albumArt {
            width: 100%;
            max-width: 64px;
        }

        .add_img {
            width: 100%;
            max-width: 30px;
        }

        .add_plus {
            background: transparent;
            height: 80px;
        }

        .searchbar {
            /*background: #212121;*/
            /*background: #030b1f;*/
            background: #595959;
            border-bottom: solid 2px;
            height: 100px;
        }

        .odd {
            /*background:#26292E;*/
            background: #33394566;
        }

        .partyName {
            margin-bottom: 5px;
            margin-top: 5px;
            font-size: 20px;
            font-weight: bold;
        }

        .inner-addon {
            position: relative;
        }

        /* style icon */
        .inner-addon .glyphicon {
            position: absolute;
            padding: 5px;
            pointer-events: none;
        }

        /* align icon */
        .left-addon .glyphicon {
            left: 0px;
        }

        .right-addon .glyphicon {
            right: 0px;
        }

        /* add padding  */
        .left-addon input {
            padding-left: 30px;
        }

        #input {
            font-size: 30px;
            font-weight: bold;
            height: 50px;
            padding-left: 60px !important;
            outline: none;
        }

        .inputimg {
            width: 100%;
            max-width: 50px;
            padding: 2px;
            margin-left: 5px;
        }

        #back_ {
            cursor: pointer;
        }

        .songinstance {
            height: 80px;
            padding-right: 0;
        }

        .songinstance_div {
            line-height: 75px;
        }

        .songinstance_div_text {
            line-height: 45px;
            height: 80px;
        }

        tr:nth-child(even) {
            background: #33394566;
        }

        .novote {

            width: 100%;
            max-width: 50px;
        }

        body {
            touch-action: manipulation;
        }

        .setP, .setP:hover, .setP:active {
            color: white;
            background: #2a3243;
        }

        .playlistelement {
            width: 50px;
            height: 50px;
        }

        .delete_song {
            width: 100%;
            max-width: 20px;
            margin-right: 25px;
            margin-left: -20px;
            transform: rotate(45deg);
        }

        .buttonInside {
            position: relative;
            margin-bottom: 10px;
        }

        /*.inputbut{*/
        /*height:25px;*/
        /*width:100%;*/
        /*padding-left:10px;*/
        /*border-radius: 4px;*/
        /*border:none;outline:none;*/
        /*}*/
        .buttoncoso {
            background: transparent;
            position: absolute;
            left: 3px;
            top: 4px;
            border: none;
            height: 40px;
            width: 40px;
            border-radius: 100%;
            outline: none;
            text-align: center;
            font-weight: bold;
            padding: 2px;
        }

        .buttoncoso:hover {
            background: transparent;
            cursor: pointer;
        }

        .playingnoyvote, .bumped {
            width: 100%;
            max-width: 50px;
        }

        #tvModeSection {
            position: absolute;
            width: 100%;
            height: 100vh;
            display: none;
            z-index: 99999;
            justify-content: center;
            align-items: center;

        }

        .tvmodebg {
            background-repeat: no-repeat;
            /*background: url('https://i.scdn.co/image/d8e32534927f05f5f8e095998d330398f150b4b7');*/
            background-size: cover;
            position: absolute;
            width: 100%;
            height: 100vh;
            filter: blur(7px);
            background-position: center;
        }

        .tvModePhoto {
            width: 90%;
            border-radius: 10px;
        }

        .currentArtist {
            font-size: 25px;
        }

        .currentTrack {
            font-size: 60px;
            font-weight: bold;
        }

        #logo_tv {
            height: 121px;
            top: 0;
            position: absolute;
            right: 0;
            width: 210px;
            border-radius: 0px 0px 0px 10px;
            text-align: center;
            background: #081733;
            box-shadow: 0px 6px 20px #00000070;
            z-index: 99999;
        }
        .joss{    width: 100%;
            max-width: 110px;
            border-radius: 100px;}



        .pyro > .before, .pyro > .after {
            position: absolute;
            width: 5px;
            height: 5px;
            border-radius: 50%;
            box-shadow: 0 0 #fff, 0 0 #fff, 0 0 #fff, 0 0 #fff, 0 0 #fff, 0 0 #fff, 0 0 #fff, 0 0 #fff, 0 0 #fff, 0 0 #fff, 0 0 #fff, 0 0 #fff, 0 0 #fff, 0 0 #fff, 0 0 #fff, 0 0 #fff, 0 0 #fff, 0 0 #fff, 0 0 #fff, 0 0 #fff, 0 0 #fff, 0 0 #fff, 0 0 #fff, 0 0 #fff, 0 0 #fff, 0 0 #fff, 0 0 #fff, 0 0 #fff, 0 0 #fff, 0 0 #fff, 0 0 #fff, 0 0 #fff, 0 0 #fff, 0 0 #fff, 0 0 #fff, 0 0 #fff, 0 0 #fff, 0 0 #fff, 0 0 #fff, 0 0 #fff, 0 0 #fff, 0 0 #fff, 0 0 #fff, 0 0 #fff, 0 0 #fff, 0 0 #fff, 0 0 #fff, 0 0 #fff, 0 0 #fff, 0 0 #fff, 0 0 #fff;
            -moz-animation: 1s bang ease-out infinite backwards, 1s gravity ease-in infinite backwards, 5s position linear infinite backwards;
            -webkit-animation: 1s bang ease-out infinite backwards, 1s gravity ease-in infinite backwards, 5s position linear infinite backwards;
            -o-animation: 1s bang ease-out infinite backwards, 1s gravity ease-in infinite backwards, 5s position linear infinite backwards;
            -ms-animation: 1s bang ease-out infinite backwards, 1s gravity ease-in infinite backwards, 5s position linear infinite backwards;
            animation: 1s bang ease-out infinite backwards, 1s gravity ease-in infinite backwards, 5s position linear infinite backwards;
        }

        .pyro > .after {
            -moz-animation-delay: 1.25s, 1.25s, 1.25s;
            -webkit-animation-delay: 1.25s, 1.25s, 1.25s;
            -o-animation-delay: 1.25s, 1.25s, 1.25s;
            -ms-animation-delay: 1.25s, 1.25s, 1.25s;
            animation-delay: 1.25s, 1.25s, 1.25s;
            -moz-animation-duration: 1.25s, 1.25s, 6.25s;
            -webkit-animation-duration: 1.25s, 1.25s, 6.25s;
            -o-animation-duration: 1.25s, 1.25s, 6.25s;
            -ms-animation-duration: 1.25s, 1.25s, 6.25s;
            animation-duration: 1.25s, 1.25s, 6.25s;
        }

        .pyro{
            display:none;
        }
        @-webkit-keyframes bang {
            to {
                box-shadow: 66px -325.6666666667px #bf00ff, 172px -411.6666666667px #40ff00, 113px -141.6666666667px #84ff00, -92px -98.6666666667px #fb00ff, -236px 2.3333333333px #0dff00, -217px -17.6666666667px #ff7300, 97px -37.6666666667px #ffbf00, -65px -303.6666666667px #c800ff, -248px -211.6666666667px #4dff00, 188px -393.6666666667px #4000ff, -58px -370.6666666667px #ff00ee, 148px -280.6666666667px #5e00ff, 192px -72.6666666667px #ff0080, -126px -240.6666666667px #84ff00, -63px 72.3333333333px #ff3c00, 88px -271.6666666667px #2fff00, -36px 9.3333333333px #0900ff, 120px -371.6666666667px #4400ff, -62px -355.6666666667px #0d00ff, -153px -392.6666666667px #ffcc00, 108px -35.6666666667px #00ffcc, 28px -328.6666666667px #00ff44, 90px -356.6666666667px #80ff00, 141px -245.6666666667px #ff0080, -109px -64.6666666667px #1100ff, -76px -88.6666666667px #ffaa00, 71px -175.6666666667px #ae00ff, 76px -147.6666666667px #00ff91, -179px -322.6666666667px #ff0080, -45px -181.6666666667px #ff00fb, -164px -190.6666666667px #ff7300, 222px -182.6666666667px #004dff, -32px -11.6666666667px #00e1ff, -10px -307.6666666667px #ff0080, 250px -139.6666666667px #00fff2, 211px -85.6666666667px #ff005e, 35px -410.6666666667px #0d00ff, -191px 46.3333333333px #ff00aa, 94px -27.6666666667px #00ff6f, -119px -339.6666666667px #ff0048, 183px -194.6666666667px #aaff00, 169px -362.6666666667px #00ff5e, -95px -209.6666666667px #00ff37, -23px -140.6666666667px #00ff8c, 88px -135.6666666667px #006fff, -180px -360.6666666667px #00ff1a, 34px -362.6666666667px #c800ff, 225px -372.6666666667px #ff0015, 247px 21.3333333333px #62ff00, -44px -368.6666666667px #ff0048, 213px -186.6666666667px #4d00ff;
            }
        }
        @-moz-keyframes bang {
            to {
                box-shadow: 66px -325.6666666667px #bf00ff, 172px -411.6666666667px #40ff00, 113px -141.6666666667px #84ff00, -92px -98.6666666667px #fb00ff, -236px 2.3333333333px #0dff00, -217px -17.6666666667px #ff7300, 97px -37.6666666667px #ffbf00, -65px -303.6666666667px #c800ff, -248px -211.6666666667px #4dff00, 188px -393.6666666667px #4000ff, -58px -370.6666666667px #ff00ee, 148px -280.6666666667px #5e00ff, 192px -72.6666666667px #ff0080, -126px -240.6666666667px #84ff00, -63px 72.3333333333px #ff3c00, 88px -271.6666666667px #2fff00, -36px 9.3333333333px #0900ff, 120px -371.6666666667px #4400ff, -62px -355.6666666667px #0d00ff, -153px -392.6666666667px #ffcc00, 108px -35.6666666667px #00ffcc, 28px -328.6666666667px #00ff44, 90px -356.6666666667px #80ff00, 141px -245.6666666667px #ff0080, -109px -64.6666666667px #1100ff, -76px -88.6666666667px #ffaa00, 71px -175.6666666667px #ae00ff, 76px -147.6666666667px #00ff91, -179px -322.6666666667px #ff0080, -45px -181.6666666667px #ff00fb, -164px -190.6666666667px #ff7300, 222px -182.6666666667px #004dff, -32px -11.6666666667px #00e1ff, -10px -307.6666666667px #ff0080, 250px -139.6666666667px #00fff2, 211px -85.6666666667px #ff005e, 35px -410.6666666667px #0d00ff, -191px 46.3333333333px #ff00aa, 94px -27.6666666667px #00ff6f, -119px -339.6666666667px #ff0048, 183px -194.6666666667px #aaff00, 169px -362.6666666667px #00ff5e, -95px -209.6666666667px #00ff37, -23px -140.6666666667px #00ff8c, 88px -135.6666666667px #006fff, -180px -360.6666666667px #00ff1a, 34px -362.6666666667px #c800ff, 225px -372.6666666667px #ff0015, 247px 21.3333333333px #62ff00, -44px -368.6666666667px #ff0048, 213px -186.6666666667px #4d00ff;
            }
        }
        @-o-keyframes bang {
            to {
                box-shadow: 66px -325.6666666667px #bf00ff, 172px -411.6666666667px #40ff00, 113px -141.6666666667px #84ff00, -92px -98.6666666667px #fb00ff, -236px 2.3333333333px #0dff00, -217px -17.6666666667px #ff7300, 97px -37.6666666667px #ffbf00, -65px -303.6666666667px #c800ff, -248px -211.6666666667px #4dff00, 188px -393.6666666667px #4000ff, -58px -370.6666666667px #ff00ee, 148px -280.6666666667px #5e00ff, 192px -72.6666666667px #ff0080, -126px -240.6666666667px #84ff00, -63px 72.3333333333px #ff3c00, 88px -271.6666666667px #2fff00, -36px 9.3333333333px #0900ff, 120px -371.6666666667px #4400ff, -62px -355.6666666667px #0d00ff, -153px -392.6666666667px #ffcc00, 108px -35.6666666667px #00ffcc, 28px -328.6666666667px #00ff44, 90px -356.6666666667px #80ff00, 141px -245.6666666667px #ff0080, -109px -64.6666666667px #1100ff, -76px -88.6666666667px #ffaa00, 71px -175.6666666667px #ae00ff, 76px -147.6666666667px #00ff91, -179px -322.6666666667px #ff0080, -45px -181.6666666667px #ff00fb, -164px -190.6666666667px #ff7300, 222px -182.6666666667px #004dff, -32px -11.6666666667px #00e1ff, -10px -307.6666666667px #ff0080, 250px -139.6666666667px #00fff2, 211px -85.6666666667px #ff005e, 35px -410.6666666667px #0d00ff, -191px 46.3333333333px #ff00aa, 94px -27.6666666667px #00ff6f, -119px -339.6666666667px #ff0048, 183px -194.6666666667px #aaff00, 169px -362.6666666667px #00ff5e, -95px -209.6666666667px #00ff37, -23px -140.6666666667px #00ff8c, 88px -135.6666666667px #006fff, -180px -360.6666666667px #00ff1a, 34px -362.6666666667px #c800ff, 225px -372.6666666667px #ff0015, 247px 21.3333333333px #62ff00, -44px -368.6666666667px #ff0048, 213px -186.6666666667px #4d00ff;
            }
        }
        @-ms-keyframes bang {
            to {
                box-shadow: 66px -325.6666666667px #bf00ff, 172px -411.6666666667px #40ff00, 113px -141.6666666667px #84ff00, -92px -98.6666666667px #fb00ff, -236px 2.3333333333px #0dff00, -217px -17.6666666667px #ff7300, 97px -37.6666666667px #ffbf00, -65px -303.6666666667px #c800ff, -248px -211.6666666667px #4dff00, 188px -393.6666666667px #4000ff, -58px -370.6666666667px #ff00ee, 148px -280.6666666667px #5e00ff, 192px -72.6666666667px #ff0080, -126px -240.6666666667px #84ff00, -63px 72.3333333333px #ff3c00, 88px -271.6666666667px #2fff00, -36px 9.3333333333px #0900ff, 120px -371.6666666667px #4400ff, -62px -355.6666666667px #0d00ff, -153px -392.6666666667px #ffcc00, 108px -35.6666666667px #00ffcc, 28px -328.6666666667px #00ff44, 90px -356.6666666667px #80ff00, 141px -245.6666666667px #ff0080, -109px -64.6666666667px #1100ff, -76px -88.6666666667px #ffaa00, 71px -175.6666666667px #ae00ff, 76px -147.6666666667px #00ff91, -179px -322.6666666667px #ff0080, -45px -181.6666666667px #ff00fb, -164px -190.6666666667px #ff7300, 222px -182.6666666667px #004dff, -32px -11.6666666667px #00e1ff, -10px -307.6666666667px #ff0080, 250px -139.6666666667px #00fff2, 211px -85.6666666667px #ff005e, 35px -410.6666666667px #0d00ff, -191px 46.3333333333px #ff00aa, 94px -27.6666666667px #00ff6f, -119px -339.6666666667px #ff0048, 183px -194.6666666667px #aaff00, 169px -362.6666666667px #00ff5e, -95px -209.6666666667px #00ff37, -23px -140.6666666667px #00ff8c, 88px -135.6666666667px #006fff, -180px -360.6666666667px #00ff1a, 34px -362.6666666667px #c800ff, 225px -372.6666666667px #ff0015, 247px 21.3333333333px #62ff00, -44px -368.6666666667px #ff0048, 213px -186.6666666667px #4d00ff;
            }
        }
        @keyframes bang {
            to {
                box-shadow: 66px -325.6666666667px #bf00ff, 172px -411.6666666667px #40ff00, 113px -141.6666666667px #84ff00, -92px -98.6666666667px #fb00ff, -236px 2.3333333333px #0dff00, -217px -17.6666666667px #ff7300, 97px -37.6666666667px #ffbf00, -65px -303.6666666667px #c800ff, -248px -211.6666666667px #4dff00, 188px -393.6666666667px #4000ff, -58px -370.6666666667px #ff00ee, 148px -280.6666666667px #5e00ff, 192px -72.6666666667px #ff0080, -126px -240.6666666667px #84ff00, -63px 72.3333333333px #ff3c00, 88px -271.6666666667px #2fff00, -36px 9.3333333333px #0900ff, 120px -371.6666666667px #4400ff, -62px -355.6666666667px #0d00ff, -153px -392.6666666667px #ffcc00, 108px -35.6666666667px #00ffcc, 28px -328.6666666667px #00ff44, 90px -356.6666666667px #80ff00, 141px -245.6666666667px #ff0080, -109px -64.6666666667px #1100ff, -76px -88.6666666667px #ffaa00, 71px -175.6666666667px #ae00ff, 76px -147.6666666667px #00ff91, -179px -322.6666666667px #ff0080, -45px -181.6666666667px #ff00fb, -164px -190.6666666667px #ff7300, 222px -182.6666666667px #004dff, -32px -11.6666666667px #00e1ff, -10px -307.6666666667px #ff0080, 250px -139.6666666667px #00fff2, 211px -85.6666666667px #ff005e, 35px -410.6666666667px #0d00ff, -191px 46.3333333333px #ff00aa, 94px -27.6666666667px #00ff6f, -119px -339.6666666667px #ff0048, 183px -194.6666666667px #aaff00, 169px -362.6666666667px #00ff5e, -95px -209.6666666667px #00ff37, -23px -140.6666666667px #00ff8c, 88px -135.6666666667px #006fff, -180px -360.6666666667px #00ff1a, 34px -362.6666666667px #c800ff, 225px -372.6666666667px #ff0015, 247px 21.3333333333px #62ff00, -44px -368.6666666667px #ff0048, 213px -186.6666666667px #4d00ff;
            }
        }
        @-webkit-keyframes gravity {
            to {
                transform: translateY(200px);
                -moz-transform: translateY(200px);
                -webkit-transform: translateY(200px);
                -o-transform: translateY(200px);
                -ms-transform: translateY(200px);
                opacity: 0;
            }
        }
        @-moz-keyframes gravity {
            to {
                transform: translateY(200px);
                -moz-transform: translateY(200px);
                -webkit-transform: translateY(200px);
                -o-transform: translateY(200px);
                -ms-transform: translateY(200px);
                opacity: 0;
            }
        }
        @-o-keyframes gravity {
            to {
                transform: translateY(200px);
                -moz-transform: translateY(200px);
                -webkit-transform: translateY(200px);
                -o-transform: translateY(200px);
                -ms-transform: translateY(200px);
                opacity: 0;
            }
        }
        @-ms-keyframes gravity {
            to {
                transform: translateY(200px);
                -moz-transform: translateY(200px);
                -webkit-transform: translateY(200px);
                -o-transform: translateY(200px);
                -ms-transform: translateY(200px);
                opacity: 0;
            }
        }
        @keyframes gravity {
            to {
                transform: translateY(200px);
                -moz-transform: translateY(200px);
                -webkit-transform: translateY(200px);
                -o-transform: translateY(200px);
                -ms-transform: translateY(200px);
                opacity: 0;
            }
        }
        @-webkit-keyframes position {
            0%, 19.9% {
                margin-top: 10%;
                margin-left: 40%;
            }
            20%, 39.9% {
                margin-top: 40%;
                margin-left: 30%;
            }
            40%, 59.9% {
                margin-top: 20%;
                margin-left: 70%;
            }
            60%, 79.9% {
                margin-top: 30%;
                margin-left: 20%;
            }
            80%, 99.9% {
                margin-top: 30%;
                margin-left: 80%;
            }
        }
        @-moz-keyframes position {
            0%, 19.9% {
                margin-top: 10%;
                margin-left: 40%;
            }
            20%, 39.9% {
                margin-top: 40%;
                margin-left: 30%;
            }
            40%, 59.9% {
                margin-top: 20%;
                margin-left: 70%;
            }
            60%, 79.9% {
                margin-top: 30%;
                margin-left: 20%;
            }
            80%, 99.9% {
                margin-top: 30%;
                margin-left: 80%;
            }
        }
        @-o-keyframes position {
            0%, 19.9% {
                margin-top: 10%;
                margin-left: 40%;
            }
            20%, 39.9% {
                margin-top: 40%;
                margin-left: 30%;
            }
            40%, 59.9% {
                margin-top: 20%;
                margin-left: 70%;
            }
            60%, 79.9% {
                margin-top: 30%;
                margin-left: 20%;
            }
            80%, 99.9% {
                margin-top: 30%;
                margin-left: 80%;
            }
        }
        @-ms-keyframes position {
            0%, 19.9% {
                margin-top: 10%;
                margin-left: 40%;
            }
            20%, 39.9% {
                margin-top: 40%;
                margin-left: 30%;
            }
            40%, 59.9% {
                margin-top: 20%;
                margin-left: 70%;
            }
            60%, 79.9% {
                margin-top: 30%;
                margin-left: 20%;
            }
            80%, 99.9% {
                margin-top: 30%;
                margin-left: 80%;
            }
        }
        @keyframes position {
            0%, 19.9% {
                margin-top: 10%;
                margin-left: 40%;
            }
            20%, 39.9% {
                margin-top: 40%;
                margin-left: 30%;
            }
            40%, 59.9% {
                margin-top: 20%;
                margin-left: 70%;
            }
            60%, 79.9% {
                margin-top: 30%;
                margin-left: 20%;
            }
            80%, 99.9% {
                margin-top: 30%;
                margin-left: 80%;
            }
        }



    </style>
</head>
<body>

@if(!Auth::user())
    <section id="splash" style="display:flex;
justify-content:center;
align-items:center; height: 100vh;background:#051735;">
        <div style="text-align:center; width:100%">
            <div class="pyro animated bounceInDown">
                <div class="before"></div>
                <div class="after"></div>
            <img src="http://digitalcoaster.mx/pp.jpg" alt="" class="joss animated pulse infinite"><br><br>
            </div>
            <img src="{{asset('/spotiparty.png')}}" alt="" style="    width: 100%; max-width: 450px;" class="animated bounceInUp spotilogo">


            <p class="love animated fadeIn">Made with <img src="{{asset('fullheart_.png')}}" alt=""
                                           style="width: 100%;max-width: 18px;"> by esg.</p>
        </div>
    </section>
@endif

<section id="tvModeSection">
    <div id="logo_tv">
        <img src="{{asset('/spotiparty.png')}}" alt="" style="     width: 100%;    max-width: 200px;">
    </div>

    <div class="tvmodebg"></div>

    <div class="col-xs-12" style="height: 55%;background: #ffffff4f;">
        <div class="col-xs-4"
             style="text-align:center;display:flex;justify-content:center;align-items:center;height: 100%;">
            <a onclick="tvMode(null,true)"><img src="" alt="" class="tvModePhoto"></a>
        </div>
        <div class="col-xs-8"
             style="text-align:left;display:flex;justify-content:center;align-items:center;height: 100%;">
            <div class="col-xs-12">
                <p class="currentTrack"></p>
                <p class="currentArtist"></p>
            </div>
        </div>


    </div>

</section>


<section id="mainSection" @if(!Auth::user()) style="display:none;" @endif>

    <div class="container-fluid">

        <div class="row" style="height: 125px;">
            <div class="col-xs-12 t_cent navbar navbar-default navbar-fixed-top searchbar" style="height: 115px; ">
                <p class="partyName">Pichifest!</p>


                <div class="buttonInside">
                    <input type="text" class="form-control inputbut" id="input" placeholder="Busca Canciones"/>

                    <button id="erase" class="buttoncoso" onclick="backtoqueue()">
                        <img src="/logo.png" alt="" class="glyphicon inputimg" id="logo_">
                    </button>
                </div>


            </div>


        </div>


        <div class="row">

            <table id="queueContainer" style="width: 100%;">
            </table>

        </div>
        <div class="row">
            <div class="col-xs-12 nopadding">
                <img src="/loader_orange.gif" alt="" class="loader_" id="loader">
            </div>
            <table id="tracksContainer" style="width: 100%;">
            </table>
        </div>

        @if(Auth::user())
            <div class="row"
                 style="width: 100%;bottom: 0;position: fixed;display: flex;justify-content: center;align-items: center;height: 100px;background: #cc7045;">
                @if(Auth::user()->spotifyToken == 'not-set')
                    <div class="col-xs-12 nopadding t_cent" id="authsp">
                        <button class="btn setP" onclick="window.location.href='/authspotify'">Auth Spotify</button>
                    </div><br>

                @endif
                <div class="col-xs-3 nopadding t_cent">
                    <button class="btn setP" onclick="setPlaylist()">Set Playlist</button>
                </div>
                <div class="col-xs-3 nopadding t_cent">
                    <button class="btn setP" onclick="spotifyApi.play()" id="btPlay">Play</button>
                    <button class="btn setP" onclick="spotifyApi.pause()" id="btPause" style="display:none;">Pause
                    </button>
                </div>
                <div class="col-xs-3 nopadding t_cent">
                    <button class="btn setP" onclick="startPlaylist()">StartPlaylist</button>
                </div>
                <div class="col-xs-3 nopadding t_cent">
                    <button class="btn setP" onclick="tvMode(finaltracks[0],true)">TvMode</button>
                </div>


            </div>
        @endif

    </div>
</section>

<!-- Modal -->
<div class="modal fade" id="topnow" role="dialog">
    <div class="modal-dialog modal-sm" style="display: flex;
    justify-content: center;
    align-items: center;
    height: 100vh;">
        <div class="modal-content">

            <div class="modal-body">
                <div class="col-xs-12">
                    <div class="col-xs-6">
                        <button class="btn btn-info" onclick="overrideStatus('bump')"> Bump</button>
                    </div>
                    <div class="col-xs-6">
                        <button class="btn btn-success" onclick="overrideStatus('now')"> Play Now</button>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>


<div id="playlist_modal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">

            <div class="modal-body">

                <div class="row" id="playlist_row">

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>


</body>


<script src="https://code.jquery.com/jquery-3.2.1.js" integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE="
        crossorigin="anonymous"></script>



<script>
    $(window).bind("load", function () {
        setTimeout(() => {
            @if(!Auth::user())
                $('.spotilogo').fadeOut(function(){
                    $('.pyro').fadeIn(function(){
                        setTimeout(() => {
                        $('#splash').fadeOut(() => {
                            $('#mainSection').fadeIn()
                        });  }, 2500);
                    })
                });
            @endif
        }, 1500)
    });
</script>

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
        integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
        crossorigin="anonymous"></script>
<script src="/src/spotify-web-api.js"></script>
<script src="https://sdk.scdn.co/spotify-player.js"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script src="https://www.gstatic.com/firebasejs/5.4.2/firebase.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fingerprintjs2/1.8.2/fingerprint2.min.js"></script>

<script>

    let spotifyApi = new SpotifyWebApi();
            @if(Auth::user())
    let deviceismaster = true;
            {{--let accessCode = '{{Auth::user()->spotifyToken}}';--}}
            @else
    let deviceismaster = false;
            @endif
    let accessCode = '{{$user->spotifyToken}}';
    let tracksContainer = $('#tracksContainer');
    let queueContainer = $('#queueContainer');
    let typingTimer;
    let doneTypingInterval = 500;
    let searchInput = document.getElementById('input');
    let loader = document.getElementById('loader');
    let extPlayer, deviceId, user_id, playlist, currentSong;
    let config = {
        apiKey: "AIzaSyBbj_8LTsaSM-k5uSuG5zFIa5_tAALxijs",
        authDomain: "spotiparty-1aed9.firebaseapp.com",
        databaseURL: "https://spotiparty-1aed9.firebaseio.com",
        projectId: "spotiparty-1aed9",
        storageBucket: "spotiparty-1aed9.appspot.com",
        messagingSenderId: "719459818655"
    };
    firebase.initializeApp(config);
    let database = firebase.database();
    let queueRef = firebase.database().ref('sessions/' + '{{$spsession->code}}');
    queueRef.on('value', function (snapshot) {
        playlist = snapshot.val();
        showPlaylist(playlist)
    });
    let finaltracks = [];
    let frommobile;
    let globalState;
    let inputImg = document.getElementById('logo_');
    let isSearching = false;
    let override_uri;
    let override_name;
    let override_artist;
    let override_album;
    let override_bigalbum;
    let istvMode = false;

    function toggleVote(uri, name, artist, albumart, changeImage, bigalbum) {
        heart = document.getElementById('heart-' + uri);
        songRef = firebase.database().ref('sessions/' + '{{$spsession->code}}' + '/' + uri);
        songRef.transaction(function (song) {
            if (song) {
                if (song.votes && song.votes[user_id]) {
                    if (changeImage && heart) {
                        console.log('change')
                        heart.src = '{{url('/emptyheart_.png')}}';
                    }

                    if (song.loveCount == 1) {
                        songRef.remove();
                        return;
                    }
                    song.loveCount--;
                    song.votes[user_id] = null;

                } else {
                    if (changeImage && heart) {
                        console.log('change');
                        heart.src = '{{url('/fullheart_.png')}}';
                    }
                    song.loveCount++;
                    if (!song.votes) {
                        song.votes = {};
                    }
                    song.votes[user_id] = true;
                }
                return song;
            }
            else {
                addSongtoQueue(uri, name, artist, albumart, false, bigalbum);
            }
        });
    }


    function addSongtoQueue(song_uri, name, artist, albumart, top, bigalbum) {
        name = name.toString().replace(/['"]/g, '\\"');
        artist = artist.toString().replace(/['"]/g, '\\"');
        songObj = {};
        songObj['addedAt'] = new Date().toUTCString();
        songObj['trackname'] = name;
        songObj['trackartist'] = artist;
        songObj['albumart'] = albumart;
        songObj['bigalbumart'] = bigalbum;
        songObj['playing'] = false;

        top === true && deviceismaster ? songObj['loveCount'] = 99 : songObj['loveCount'] = 1;
        votesObj = {};
        votesObj[user_id] = true;
        songObj['votes'] = votesObj;
        firebase.database().ref('sessions/' + '{{$spsession->code}}' + '/' + song_uri).set(songObj);
    }


    $(document).ready(function () {


        $('.playlistchoose').css('width', window.innerWidth + 'px');
        axios.defaults.headers.common['X-CSRF-TOKEN'] = document.querySelector('meta[name="csrf-token"]').getAttribute('content');
        refreshToken();
        console.log('------Token Refreshed------');


        // var options = {}
        // new Fingerprint2.getV18(options, function (result, components) {
        //     // result is murmur hash fingerprint
        //     // components is array of {key: 'foo', value: 'component value'}
        //     console.log('')
        //
        // });
        new Fingerprint2().get(function (result) {
                   user_id = result;

        });
        searchInput.addEventListener('keydown', () => {
            if (searchInput.value) {
                loader.style.display = 'block';
            }
        });
        searchInput.addEventListener('keyup', () => {
            $('#queueContainer').hide();
            clearTimeout(typingTimer);
            if (searchInput.value) {
                inputImg.src = '/back.png';
                typingTimer = setTimeout(doneTyping, doneTypingInterval);
            }
            else {
                $('#loader').fadeOut(() => {
                    isSearching = false;
                    $('#queueContainer').show();
                    $('#tracksContainer').hide();
                });
            }
        });

        function doneTyping() {
            tracksContainer.empty();
            inputImg.src = '/back.png';
            isSearching = true;
            search_();
        }

        setInterval(function () {
            refreshToken();
        }, 300000);

        window.onbeforeunload = function () {
            if (deviceismaster) {
                return "Si recargas se pausará y se perderá la cancion en curso";
            }
        };
        if (deviceismaster) {
            frommobile = window.mobileAndTabletcheck();
            console.log(`----Mobile: ${frommobile} ----`);
        }
        if (frommobile) {
            $('#authsp').hide();
        }

    });

    window.mobileAndTabletcheck = function () {
        var check = false;
        (function (a) {
            if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino|android|ipad|playbook|silk/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0, 4))) check = true;
        })(navigator.userAgent || navigator.vendor || window.opera);
        return check;
    };

    window.onSpotifyWebPlaybackSDKReady = () => {
        const token = accessCode;
        const player = new Spotify.Player({
            name: 'ElgalSpotiParty',
            getOAuthToken: cb => {
                cb(token);
            }
        });
        extPlayer = player;
        player.addListener('ready', ({device_id}) => {
            deviceId = device_id;
            setDeviceId(device_id);
            console.log('Ready with Device ID', device_id);
        });
        player.addListener('not_ready', ({device_id}) => {
            console.log('Device ID has gone offline', device_id);
            alert('No hay red');
        });

        player.addListener(
            'player_state_changed', state => {
                console.log('estado', state);
                globalState = state;
                // if (deviceismaster) {
                //     getPlayState();
                // }
                if (this.state && !this.state.paused && state.paused && state.position === 0) {
                    console.log('Track ended');
                    next_track(state.track_window.current_track.uri)
                }
                this.state = state;
            }
        );

        player.connect();
    };


    function setDeviceId(dev) {
        axios.post('/setdeviceid', {
            ses_id: '{{$spsession->id}}',
            device_id: dev,
        })
            .then(function (response) {
                console.log(response);
            })
            .catch(function (error) {
                console.log(error);
            });
    }


    function search_() {
        console.log('------ SEARCH -------');
        $('#add_plus').off();
        options = {
            type: 'track',
            market: 'MX',
            limit: '30'
        };
        spotifyApi.searchTracks($('#input').val(), options)
            .then(function (data) {
                let tracks = data.tracks.items;
                $('#loader').fadeOut(() => {
                    $(tracks).each((index, item) => {
                        let albumart = item.album.images[2].url;
                        let bigalbumart = item.album.images[0].url;
                        let uri = item.uri;
                        let name = item.name;
                        let artist = '';
                        if (item.artists.length === 1) {
                            artist = item.artists[0].name;
                        }
                        else {
                            $(item.artists).each((index_, artista) => {
                                if (index_)
                                    index_ < item.artists.length - 1 ? artist += artista.name + ' feat. ' : artist = artist += artista.name;
                            });
                        }

                        if (window.innerWidth <= 425) {

                            if (name.length > 16) {
                                name = name.substring(0, 16) + '...';
                            }
                            if (artist.length > 16) {
                                artist = artist.substring(0, 16) + '...';
                            }
                        }
                        //
                        //
                        //
                        //
                        //
                        //
                        //
                        //
                        //
                        //
                        // item[1].votes.hasOwnProperty(user_id) ? heart_ = '/fullheart_.png' : heart_ = '/emptyheart_.png'

                        track_html = `<tr>
                        <td><div class="col-xs-12 songinstance">
                    <div class="col-xs-2 col-sm-1 nopadding t_cent songinstance_div">
                        <img src="${albumart}" alt="albumArt" class="albumArt">
                    </div>
                    <div class="col-xs-8 songinstance_div_text">
                        <div class="col-xs-12">
                            <p class="nomargin songtitle">${name}</p>
                        </div>
                        <div class="col-xs-12">
                            <p>${artist} </p>
                        </div>
                    </div>
                     <div class="col-xs-2 col-sm-3 nopadding t_cent">
                        <button trackuri = '${uri}' albumart='${albumart}' trackname='${name}' trackartist='${artist}' bigalbumart='${bigalbumart}' class="add_plus btn">
                        <img src="/add_.png" alt="add" class="add_img"  trackuri = '${uri}' albumart='${albumart}' bigalbumart='${bigalbumart}' trackname='${name}' trackartist='${artist}'>
                        </button>
                    </div>
                </div></td></tr>`;
                        tracksContainer.append(track_html);
                    });
                    $('.add_plus,.addimg').click(function (event) {
                        uri_ = $(event.target).attr('trackuri');
                        name_ = $(event.target).attr('trackname');
                        artist_ = $(event.target).attr('trackartist');
                        albumart_ = $(event.target).attr('albumart');
                        bigalbumart_ = $(event.target).attr('bigalbumart');
                        $('#tracksContainer').hide();
                        $('#queueContainer').fadeIn('fast');
                        $('#input').val('');
                        toggleVote(uri_, name_, artist_, albumart_, false, bigalbumart_);
                    });
                });
            }, function (err) {
                console.error(err);
            });
        tracksContainer.fadeIn('fast');
    }


    function next_track(prev) {
        // when ended, remove from playlist
        console.log(prev)
        let songref = firebase.database().ref('sessions/' + '{{$spsession->code}}' + '/' + prev);
        songref.remove();
        startPlaylist();
        console.log('next track on playlist')
    }

    function startPlaylist() {
        let track = finaltracks[0];
        let songref = firebase.database().ref('sessions/' + '{{$spsession->code}}' + '/' + track[0]);
        tvMode(finaltracks[0], false, true);
        if (!frommobile) {
            optionsPlay = {
                device_id: deviceId,
                uris: [track[0]],
                offset: {position: 0},
                position_ms: 0
            };
            spotifyApi.play(optionsPlay)
                .then(function (data) {
                    console.log('---Start Playing Tracks---');

                    songref.transaction(function (song) {
                        if (song) {
                            song.playing = true;
                            return song;
                        }

                    });
                }, function (err) {
                    console.error(err);
                });
        }
        else {
            axios.get('{{url('/')}}/playfrommobile/' + '{{$spsession->device_id}}' + '/' + track[0].toString())
                .then(function (response) {
                    songref.transaction(function (song) {
                        if (song) {
                            song.playing = true;
                            return song;
                        }
                    }).catch(function (error) {
                        console.log(error);
                    });
                });
        }
    }


    function refreshToken() {
        axios.get('/rtoken')
            .then(function (response) {
                spotifyApi.setAccessToken(response.data.user.spotifyToken);
            })
            .catch(function (error) {
                console.log(error);
            });
    }


    function showPlaylist(playlist) {
        $('.novote').off();
        $('.delete_song').off();
        $('.masterimg').off();
        queueContainer.empty();
        var alltracks = [];
        finaltracks = [];
        //1 All tracks in an array
        for (var i in playlist) {
            alltracks.push([i, playlist[i]]);
        }

        let playingTrack = [];
        playingTrack = alltracks.filter((item) => {
            return item[1].playing === true;
        });
        alltracks = alltracks.filter((item) => {
            return item[1].playing === false;
        });

        console.log('----notplayingtracks----');
        console.log(alltracks);


        console.log('----Playing track----');
        console.log(playingTrack);

        //2 all tracks have same number of votes
        let sameVotes = alltracks.every(function (el, index, arr) {
            if (index === 0) {
                return true;
            }
            else {
                return (el[1].loveCount === arr[index - 1][1].loveCount);
            }
        });
        //if 2 sort everything by date
        if (sameVotes) {
            if (playingTrack.length == 0) {
                console.log('nohay playing track')
                finaltracks = alltracks.sort(function (a, b) {
                    return a[1].addedAt.toString().localeCompare(b[1].addedAt.toString());
                });
            }
            else {
                console.log('sihay playing track')

                let pref = alltracks.sort(function (a, b) {
                    return a[1].addedAt.toString().localeCompare(b[1].addedAt.toString());
                });
                finaltracks = playingTrack.concat(pref)
                console.log(finaltracks)
                console.log('--------')
            }

        }
        else {
            //if not 2, split tracks by votes
            let sortedByVotes = alltracks.filter((item) => {
                return item[1].loveCount > 1 && item[1].playing === false;
            }).sort(function (a, b) {
                return b[1].loveCount.toString().localeCompare(a[1].loveCount.toString());
                // return b[1].loveCount.localeCompare(a[1].loveCount);
                // return parseInt(b[1].loveCount).localeCompare(parseInt(a[1].loveCount));
            });
            //if all tracks sorted by votes have the same number of votes, sort by date
            let sameVotes_ = sortedByVotes.every(function (el, index, arr) {
                if (index === 0) {
                    return true;
                }
                else {
                    return (el[1].loveCount === arr[index - 1][1].loveCount);
                }
            });
            if (sameVotes_) {
                sortedByVotes = sortedByVotes.sort(function (a, b) {
                    return a[1].addedAt.toString().localeCompare(b[1].addedAt.toString());
                });
            }
            //sort all tracks with 1 vote by date
            let sortedByDate = alltracks.filter((item) => {
                return item[1].loveCount <= 1 && item[1].playing === false;

            }).sort(function (a, b) {
                return a[1].addedAt.toString().localeCompare(b[1].addedAt.toString());
            });


            if (playingTrack.length == 0) {
                finaltracks = sortedByVotes.concat(sortedByDate);
            }
            else {
                let pref = sortedByVotes.concat(sortedByDate);
                finaltracks = playingTrack.concat(pref)

            }

        }


        $(finaltracks).each((index, item) => {
            artist_ = item[1].trackartist.toString().replace(/\\"/g, '"');
            artist_ = artist_.toString().replace(/\\'/g, "'");
            trackname_ = item[1].trackname.toString().replace(/\\"/g, '"');
            trackname_ = trackname_.toString().replace(/\\'/g, "'");
            if (window.innerWidth <= 425) {
                if (trackname_.length > 16) {
                    trackname_ = trackname_.substring(0, 16) + '...';
                }
                if (artist_.length > 16) {
                    artist_ = artist_.substring(0, 16) + '...';
                }
            }
            if (window.innerWidth <= 320) {
                if (trackname_.length > 16) {
                    trackname_ = trackname_.substring(0, 13) + '...';
                }
            }
            track_html = `<tr><td><div class="col-xs-12 songinstance">
                    <div class="col-xs-2 col-sm-1 nopadding t_cent songinstance_div">
                        <img src="${item[1].albumart}" alt="albumArt" class="albumArt">
                    </div>
                    <div class="col-xs-8 songinstance_div_text">
                        <div class="col-xs-12">
                            <p class="nomargin songtitle">${trackname_}</p>
                        </div>
                        <div class="col-xs-12">
                            <p class="artist">${artist_} · <span> ${item[1].loveCount} ${item[1].loveCount > 1 ? 'votos' : 'voto'}</span></p>
                        </div>
                    </div>
                     <div class="col-xs-2 col-sm-3 nopadding t_cent">
                        <p class="addtokiu"> <img src="/emptyheart_.png" alt="novote" class="novote" trackuri = '${item[0]}' albumart='${item[1].albumart}' trackname='${trackname_}' trackartist='${artist_}' id="heart-${item[0]}"></p>
                    </div>
                </div></td></tr>`;


            // if(item[1].loveCount > 1 &&){
            let tr = document.createElement('tr');
            let td = document.createElement('td');
            let div_one = document.createElement('div'); // songinstance
            div_one.className = 'col-xs-12 songinstance';
            let div_one_nestingDiv = document.createElement('div'); //songinstance_div
            div_one_nestingDiv.className = 'col-xs-2 col-sm-1 nopadding t_cent songinstance_div';
            let img = document.createElement('img'); //albumArt
            img.src = item[1].albumart;
            img.className = 'albumArt masterimg';
            img.setAttribute('alt', 'albumArt');
            img.setAttribute('trackuri', item[0]);
            img.setAttribute('albumart', item[1].albumart);
            img.setAttribute('bigalbumart', item[0].albumart);
            img.setAttribute('trackname', trackname_);
            img.setAttribute('trackartist', artist_);
            let div_two = document.createElement('div'); //songinstance_div_text
            div_two.className = 'col-xs-7 col-sm-8 songinstance_div_text';
            let div_two_nestingDiv_one = document.createElement('div');
            div_two_nestingDiv_one.className = 'col-xs-12';
            let p_element = document.createElement('p');
            p_element.className = 'nomargin songtitle';
            p_element.innerText = trackname_;
            let div_two_nestingDiv_two = document.createElement('div'); //col-xs-12
            div_two_nestingDiv_two.className = 'col-xs-12';
            let div_two_p = document.createElement('p');
            div_two_p.className = 'artist';
            div_two_p.innerText = artist_;
            let div_two_p_span = document.createElement('span');
            // div_two_p_span.innerText = item[1].loveCount + '' + (item[1].loveCount > 1 ? ' votos' : ' voto');
            let div_three = document.createElement('div');
            div_three.className = 'col-xs-3 col-sm-3 nopadding t_cent';
            let div_three_p = document.createElement('p');
            div_three_p.className = 'addtokiu';
            let div_three_p_img = document.createElement('img');

            if (item[1].playing) {
                div_three_p_img.src = '/play_.png';
                div_three_p_img.className = 'playingnoyvote';
            }
            else {

                if (item[1].loveCount == 99) {
                    div_three_p_img.src = '/bumped.png';
                    div_three_p_img.className = 'bumped';
                } else {
                    div_three_p_img.src = item[1].votes.hasOwnProperty(user_id) ? heart_ = '/fullheart_.png' : heart_ = '/emptyheart_.png';
                    div_three_p_img.className = 'novote';
                }
            }


            div_three_p_img.setAttribute('alt', 'novote');
            div_three_p_img.setAttribute('trackuri', item[0]);
            div_three_p_img.setAttribute('albumart', item[1].albumart);
            div_three_p_img.setAttribute('bigalbumart', item[0].albumart);
            div_three_p_img.setAttribute('trackname', trackname_);
            div_three_p_img.setAttribute('trackartist', artist_);
            div_three_p_img.setAttribute('id', "heart-" + item[0]);
            let div_three_p_img_delete = document.createElement('img');
            div_three_p_img_delete.src = 'add_.png';
            div_three_p_img_delete.className = 'delete_song';
            div_three_p_img_delete.setAttribute('alt', 'novote');
            div_three_p_img_delete.setAttribute('trackuri', item[0]);
            div_three_p_img_delete.setAttribute('albumart', item[1].albumart);
            div_three_p_img_delete.setAttribute('trackname', trackname_);
            div_three_p_img_delete.setAttribute('trackartist', artist_);
            div_one_nestingDiv.appendChild(img);
            div_two_nestingDiv_one.appendChild(p_element);
            div_two_p.appendChild(div_two_p_span);
            div_two_nestingDiv_two.appendChild(div_two_p);
            div_two.appendChild(div_two_nestingDiv_one);
            div_two.appendChild(div_two_nestingDiv_two);
            let votes_ = document.createElement('p');
            if (item[1].playing) {
                votes_.innerText = 'Playing';
            }
            else {
                if (item[1].loveCount == 99) {
                    votes_.innerText = 'Bumped'
                }
                else {
                    votes_.innerText = item[1].loveCount + '' + (item[1].loveCount > 1 ? ' votos' : ' voto');
                }
            }
            if (deviceismaster == true) {
                if (!item[1].playing) {
                    div_three.appendChild(div_three_p_img_delete);
                }
            }
            div_three.appendChild(div_three_p_img);
            div_three.appendChild(votes_);
            div_one.appendChild(div_one_nestingDiv);
            div_one.appendChild(div_two);
            div_one.appendChild(div_three);
            td.appendChild(div_one);
            tr.appendChild(td);
            js_queueContainer = document.getElementById('queueContainer');
            js_queueContainer.appendChild(tr);
            // }
        });


        $('.novote').click(function (event) {
            uri_ = $(event.target).attr('trackuri');
            name_ = $(event.target).attr('trackname');
            artist_ = $(event.target).attr('trackartist');
            albumart_ = $(event.target).attr('albumart');
            bigalbumart_ = $(event.target).attr('bigalbumart');
            toggleVote(uri_, name_, artist_, albumart_, true, event.target, bigalbumart_);
        });
        $('.delete_song').click(function (event) {
            uri_ = $(event.target).attr('trackuri');
            name_ = $(event.target).attr('trackname');
            artist_ = $(event.target).attr('trackartist');
            albumart_ = $(event.target).attr('albumart');
            bigalbumart_ = $(event.target).attr('bigalbumart');
            deleteSong(uri_);
        });


        $('.masterimg').click(function (event) {
            override_uri = $(event.target).attr('trackuri');
            override_name = $(event.target).attr('trackname');
            override_artist = $(event.target).attr('trackartist');
            override_album = $(event.target).attr('albumart');
            override_bigalbum = $(event.target).attr('bigalbumart');
            if (deviceismaster) {
                overrideStatus('first');
            }
        });
    }


    function overrideStatus(type) {
        if (type === 'first') {
            $('#topnow').modal()
        }
        if (type === 'bump') {
            if (confirm('To Top?')) {
                addSongtoQueue(override_uri, override_name, override_artist, override_album, true);
            }
        }
        if (type === 'now') {
            let currenttrack = finaltracks.filter((item) => {
                return item[1].playing === true;
            });
            if (currenttrack.length == 0) {
                addSongtoQueue(override_uri, override_name, override_artist, override_album, true);
                setTimeout(() => {
                    startPlaylist()
                }, 500);
            }
            else {
                //si si hay song playing

                track_ref = finaltracks.filter((item) => {
                    return item[1].playing == true;
                });

                let currentrackRef = firebase.database().ref('sessions/' + '{{$spsession->code}}' + '/' + track_ref[0][0]);
                currentrackRef.transaction(function (song) {
                    if (song) {
                        song.playing = false;
                        return song;
                    }
                }).then(function (data) {
                    addSongtoQueue(override_uri, override_name, override_artist, override_album, true);
                    setTimeout(() => {
                        startPlaylist()
                    }, 500);
                })
            }
        }
    }

    function deleteSong(uri) {

        if (confirm("Borrar canción?")) {
            songRef = firebase.database().ref('sessions/' + '{{$spsession->code}}' + '/' + uri);

            songRef.remove()
        }
    }


    function setPlaylist() {
        spotifyApi.getUserPlaylists().then(function (data) {
            var playlists = data.items;
            $(playlists).each((index, item) => {
                let playlist_art = item.images[0].url;
                let uri = item.id;
                let name = item.name;
                name = name.substring(0, 13) + '...';

                plist_html = `
<div class="col-xs-6 col-sm-4" style="height:210px;">
<div class="col-xs-12">
    <img src="${playlist_art}" alt="albumArt" class="plisart" style="width: 100%">
</div>
<div class="col-xs-12" style="text-align:center;">
<p style="color:black;">${name}</p>
<button class="btn btn-info" onclick="addplist('${uri}')">Add</button>
</div>
</div>`;
                $('#playlist_row').append(plist_html);
            });
            $('#playlist_modal').modal();

        }, function (err) {
            console.error(err);
        });
    }

    function backtoqueue() {
        if (isSearching) {
            console.log('enter');
            $('#input').val('');
            $('#tracksContainer').fadeOut('fast', () => {
                inputImg.src = '/logo.png';
                $('#queueContainer').show();
                isSearching = false;

            })
        }
    }

    function getPlayState() {
        if (globalState.paused != true) {
            $('#btPlay').fadeOut('fast', () => {
                $('#btPause').fadeIn('fast');
            })
        }
        else {
            $('#btPause').fadeOut('fast', () => {
                $('#btPlay').fadeIn('fast');
            })
        }
    }

    function addplist(uri, type) {
        if (confirm('Add all?')) {
            let tracks = spotifyApi.getPlaylistTracks(uri).then(function (data) {
                console.log(data)

                $(data.items).each(function (index, item) {
                    addSongtoQueue(item.track.uri, item.track.name, item.track.artists[0].name, item.track.album.images[0].url, false);
                });
            })
        }


    }


    function tvMode(track, activate, next) {
        console.log(track);


        if (activate) {
            if (!istvMode) {
                $('#tvModeSection').css('display', 'flex');
                istvMode = true
            }
            else {
                $('#tvModeSection').fadeOut();
                istvMode = false
            }
        }
        $('.tvModePhoto').attr('src',track[1].bigalbumart);
        $('.tvmodebg').css('background-image',"url('"+track[1].bigalbumart+"')");
        $('.currentTrack').text(track[1].trackname);
        $('.currentArtist').text(track[1].trackartist);

    }

</script>
</html>

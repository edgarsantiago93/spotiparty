<?php

namespace App\Http\Controllers;

use App\SessionTrack;
use App\SpSession;
use App\User;
use App\WordPool;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Auth;
use SpotifyWebAPI\Session;
use SpotifyWebAPI\SpotifyWebAPI;

use Illuminate\Http\Request;

//use SpotifyWebAPI\Session;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;


    public function mainview()
    {
        return view('main_');
//        return view('spoti_');
    }


    public function newSession()
    {
//        if (!Auth::user()) {
//            return redirect('login');
//        }

        $user = User::where('email','edgar@santiagoguerrero.mx')->first();
        $existing = SpSession::where('user_id', $user->id)->first();
        if (!$existing) {
            $session = new SpSession();
            $session->user_id = $user->id;
            $session->code = strtolower(WordPool::inRandomOrder()->first()->word) . rand(100, 1000);
            $session->device_id = 'not-set';
            $session->devide_name = 'Elgal';
            $session->img = 'coso';
            $session->active = 1;
            $session->save();
            $this->authSpotify();

            return view('spoti_')->with('spsession', $session)->with('role', 'master')->with('user', $user);
        } else {
            return view('spoti_')->with('spsession', $existing)->with('role', 'master')->with('user', $user);
        }
    }


    public function joinSession($id = null)
    {
        $session = SpSession::where('code', $id)->where('active', 1)->first();
        $res = array();
        if ($session) {
            $tracks = SessionTrack::where('session_id', $session->id)->where('played', 0)->orderBy('votes', 'DESC')->get();
            dump($tracks);
        }
    }

    public function initial()
    {
        $user = Auth::user();
        if ($user) {
            $this->authSpotify();
        } else {
            return redirect('/login');
        }

    }

    public function spotifySession()
    {
        $spotifySession = new \SpotifyWebAPI\Session(
            '49ca61d6e2374c9881b248b7b650e967',
            '335cee9ed53a452baa8fb6fac4ce1983',
            'https://spotiparty.mx/callback'
        );
        return $spotifySession;
    }

    public function authSpotify()
    {

        $user = Auth::user();
        if (!$user) {
            return redirect('/login');
        }

        $session = $this->spotifySession();
        $options = [
            'scope' => [
                'playlist-read-private',
                'user-read-currently-playing',
                'user-modify-playback-state',
                'user-read-playback-state',
                'user-library-read',
                'user-library-modify',
                'app-remote-control',
                'streaming',
                'user-read-private',
                'user-read-birthdate',
                'user-read-email',
                'playlist-read-private',
                'playlist-read-collaborative',
                'playlist-modify-public',
                'playlist-modify-private',
            ],
        ];

        header('Location: ' . $session->getAuthorizeUrl($options));
        die();
    }


    public function spotifyCallback()
    {
        $user = Auth::user();
        $session = $this->spotifySession();
        $session->requestAccessToken($_GET['code']);
        $accessToken = $session->getAccessToken();
        $refreshToken = $session->getRefreshToken();

        $user->spotifyToken = $accessToken;
        $user->spotifyRememberToken = $refreshToken;
        $user->save();

        return redirect('/');
    }


    public function refreshToken()
    {
        $user = User::where('email','edgar@santiagoguerrero.mx')->first();
        $res = array();
        if (!$user) {
            $res['status'] = '201';
            $res['message'] = 'no-user';
            return response()->json($res);
        }
        $session = $this->spotifySession();
        $session->refreshAccessToken($user->spotifyRememberToken);
        $accessToken = $session->getAccessToken();
        $user->spotifyToken = $accessToken;
        $user->spotifyRememberToken = $session->getRefreshToken();
        $user->save();
        $res['status'] = '200';
        $res['message'] = 'token-refresh';
        $res['user'] = $user;
        return response()->json($res);
    }

    public function master_player()
    {

    }

    public function slave_player(Request $r)
    {
        $ss = SpSession::where('code', $r->code)->first();
    }

    public function setDeviceId(Request $request)
    {
        $session = SpSession::find($request->ses_id);
        $session->device_id = $request->device_id;
        $session->save();
        $res = array('status' => 'ok');
        return response()->json($res);
    }


    public function playfrommobile($deviceId,$uri)
    {
        $user = User::where('email','edgar@santiagoguerrero.mx')->first();
        $api = new SpotifyWebAPI();
        $api->setAccessToken($user->spotifyToken);
        $api->play($deviceId, [
            'uris' => [$uri],
        ]);
        return response('ok',200);
    }
}



<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/




//Route::get('/', 'Controller@mainview');

Route::get('/', 'Controller@newSession');

Route::get('/s', 'Controller@initial');
Route::get('/authspotify', 'Controller@authSpotify');
Route::get('/callback', 'Controller@spotifyCallback');
Auth::routes();
Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/join_session', 'Controller@joinSession')->name('join_session');
Route::get('/new_session', 'Controller@newSession')->name('new_session');
Route::post('/setdeviceid','Controller@setDeviceId');
Route::post('/s/{id}','Controller@redirectId');


Route::get('/rtoken', 'Controller@refreshToken');

Route::get('/playfrommobile/{deviceId}/{uri}', 'Controller@playfrommobile');









